var PORT = process.env.PORT || 3000;
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

// it will start listening events
io.on('connection' ,function(socket){
    console.log('User Connected using Socket.io');

    socket.on('message', function (message) {  // listen the event message
       console.log('Message Received ' + message.text); // it will get the msg from the front end on the event message
        socket.broadcast.emit('message', message); // send msg to everyone on event messsage,
        // try to socket.emit('message',text:"hello"); from the browser console
        //it will fire the event from the front end then the text will go to server and it will broadcasted
    });

    socket.emit('message', {
        text : 'Welcome to the chat application'
    });
});

http.listen(PORT, function () {
    console.log('server is listening on port : ' + PORT);
});

