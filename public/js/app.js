var socket = io();

//front end connection event

socket.on('connect', function () {
    console.log('Connected to SocketIO server');
});

// this will take the msg from the server and sends to every one. name of the event is message. 

socket.on('message', function(message){
    console.log('New message : ');
    console.log(message.text);
});

// Handles submitting of new messages

var $form = jQuery('#message-form');

$form.on('submit', function (event) {
    event.preventDefault();  // stops the event default behaviour

    socket.emit('message', {
        text : $form.find('input[name=message]').val()
    });

    $form[0].reset().focus(); // or $form[0].val('');
});


